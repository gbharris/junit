import Foundation

public enum Error: Swift.Error {
	case unexpectedElement(elementName: String)
	case invalidRoot(elementName: String)
	case requiredAttributeNotFound(attributeName: String, elementName: String)
	case unknownFailure
}
