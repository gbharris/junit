import Foundation

class Parser: NSObject {
	
	private let url: URL
	fileprivate var parser: XMLParser?
	fileprivate var contexts: [Context] = []

	fileprivate var startTime: CFAbsoluteTime?
	fileprivate var endTime: CFAbsoluteTime?
	
	fileprivate var parsingError: Swift.Error?
	
	fileprivate var testSuites: JUnit.Model.TestSuites?
	
	init(url: URL) {
		self.url = url
		self.parser = XMLParser(contentsOf: url)
		
		super.init()
		
		self.parser?.delegate = self
	}
	
	
	func parse() throws -> JUnit.Model.TestSuites {
		self.parser?.parse()
		if let err = parsingError {
			throw err
		}
		guard let suites = testSuites else {
			throw JUnit.Error.unknownFailure
		}
		return suites
	}
	
	
	fileprivate struct Context {
		enum Element: String {
			case testsuites
			case testsuite
			case testcase
			case failure
		}
		
		var element: Element
		var attributes: [String : String]
		var content: String
		var children: [JUnitModel] = []
		
		init(elementName: String, attributes: [String : String], content: String? = nil) throws {
			guard let element = Element(rawValue: elementName) else {
				throw JUnit.Error.unexpectedElement(elementName: elementName)
			}
			self.element = element
			self.attributes = attributes
			self.content = content ?? ""
		}
	}
	
}


extension Parser: XMLParserDelegate {
	
	func parserDidStartDocument(_ parser: XMLParser) {
		startTime = CFAbsoluteTimeGetCurrent()
	}
	
	
	func parserDidEndDocument(_ parser: XMLParser) {
		endTime = CFAbsoluteTimeGetCurrent()
	}
	
	
	func parser(_ parser: XMLParser,
	            didStartElement elementName: String,
	            namespaceURI: String?,
	            qualifiedName qName: String?,
	            attributes attributeDict: [String : String] = [:]) {
		do {
			let ctx = try Context(elementName: elementName, attributes: attributeDict)
			contexts.append(ctx)
		}
		catch {
			parsingError = error
			parser.abortParsing()
		}
	}
	
	
	func parser(_ parser: XMLParser,
	            didEndElement elementName: String,
	            namespaceURI: String?,
	            qualifiedName qName: String?) {
		let ctx = contexts.removeLast()
		var parentCtx = contexts.isEmpty ? nil : contexts.removeLast()
		switch ctx.element {
		case .testsuites:
			guard let name = ctx.attributes[JUnit.Model.TestSuites.nameKey] else {
				parsingError = JUnit.Error.requiredAttributeNotFound(attributeName: JUnit.Model.TestSuites.nameKey, elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			guard let testCountStr = ctx.attributes[JUnit.Model.TestSuites.testCountKey], let testCount = UInt(testCountStr) else {
				parsingError = JUnit.Error.requiredAttributeNotFound(attributeName: JUnit.Model.TestSuites.testCountKey, elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			guard let failureCountStr = ctx.attributes[JUnit.Model.TestSuites.failureCountKey], let failureCount = UInt(failureCountStr) else {
				parsingError = JUnit.Error.requiredAttributeNotFound(attributeName: JUnit.Model.TestSuites.failureCountKey, elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			let suites = ctx.children.flatMap { $0 as? JUnit.Model.TestSuite }
			testSuites = JUnit.Model.TestSuites(name: name, testCount: testCount, failureCount: failureCount, suites: suites)

		case .testsuite:
			guard parentCtx != nil else {
				parsingError = JUnit.Error.invalidRoot(elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			guard let name = ctx.attributes[JUnit.Model.TestSuite.nameKey] else {
				parsingError = JUnit.Error.requiredAttributeNotFound(attributeName: JUnit.Model.TestSuite.nameKey, elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			guard let testCountStr = ctx.attributes[JUnit.Model.TestSuite.testCountKey], let testCount = UInt(testCountStr) else {
				parsingError = JUnit.Error.requiredAttributeNotFound(attributeName: JUnit.Model.TestSuite.testCountKey, elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			guard let failureCountStr = ctx.attributes[JUnit.Model.TestSuite.failureCountKey], let failureCount = UInt(failureCountStr) else {
				parsingError = JUnit.Error.requiredAttributeNotFound(attributeName: JUnit.Model.TestSuite.failureCountKey, elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			let cases = ctx.children.flatMap { $0 as? JUnit.Model.TestCase }
			let testSuite = JUnit.Model.TestSuite(name: name, testCount: testCount, failureCount: failureCount, cases: cases)
			parentCtx!.children.append(testSuite)
			contexts.append(parentCtx!)

		case .testcase:
			guard parentCtx != nil else {
				parsingError = JUnit.Error.invalidRoot(elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			guard let className = ctx.attributes[JUnit.Model.TestCase.classNameKey] else {
				parsingError = JUnit.Error.requiredAttributeNotFound(attributeName: JUnit.Model.TestCase.classNameKey, elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			guard let name = ctx.attributes[JUnit.Model.TestCase.nameKey] else {
				parsingError = JUnit.Error.requiredAttributeNotFound(attributeName: JUnit.Model.TestCase.nameKey, elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			
			var time: Double? = nil
			if let timeStr = ctx.attributes[JUnit.Model.TestCase.timeKey] {
				time = Double(timeStr)
			}
			
			let failures = ctx.children.flatMap { $0 as? JUnit.Model.Failure }

			let testCase = JUnit.Model.TestCase(className: className, name: name, time: time, failures: failures)
			parentCtx!.children.append(testCase)
			contexts.append(parentCtx!)

		case .failure:
			guard parentCtx != nil else {
				parsingError = JUnit.Error.invalidRoot(elementName: ctx.element.rawValue)
				parser.abortParsing()
				return
			}
			guard let message = ctx.attributes[JUnit.Model.Failure.messageKey] else {
					parsingError = JUnit.Error.requiredAttributeNotFound(attributeName: JUnit.Model.Failure.messageKey, elementName: ctx.element.rawValue)
					parser.abortParsing()
					return
			}
			let failure = JUnit.Model.Failure(message: message, content: ctx.content)
			parentCtx!.children.append(failure)
			contexts.append(parentCtx!)
			break
		}
	}
	
	
	func parser(_ parser: XMLParser, foundCharacters string: String) {
		var ctx = contexts.removeLast()
		ctx.content.append(string)
		contexts.append(ctx)
	}
	
}
