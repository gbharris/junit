import Foundation

internal protocol JUnitModel {}

public struct Model {
	public struct TestSuites: JUnitModel {
		let name: String
		let testCount: UInt // includes failures
		let failureCount: UInt

		let suites: [TestSuite]

		static let nameKey = "name"
		static let testCountKey = "tests"
		static let failureCountKey = "failures"
	}
	
	
	public struct TestSuite: JUnitModel {
		let name: String
		let testCount: UInt
		let failureCount: UInt

		let cases: [TestCase]
		
		static let nameKey = "name"
		static let testCountKey = "tests"
		static let failureCountKey = "failures"
	}
	
	
	public struct TestCase: JUnitModel {
		let className: String
		let name: String
		let time: Double?
		
		let failures: [Failure]
		
		static let classNameKey = "classname"
		static let nameKey = "name"
		static let timeKey = "time"
	}
	
	
	public struct Failure: JUnitModel {
		let message: String
		let content: String
		
		static let messageKey = "message"
	}
}
