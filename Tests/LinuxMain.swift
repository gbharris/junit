import XCTest
@testable import JUnitTests

XCTMain([
    testCase(JUnitTests.allTests),
])
