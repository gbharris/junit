import XCTest
@testable import JUnit

class ParserTests: XCTestCase {
	
	func testParsing() {
	    let tempURL: URL
		if #available(macOS 10.12, *) {
			tempURL = FileManager.default.temporaryDirectory.appendingPathComponent(UUID().uuidString)
		} else {
			tempURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(UUID().uuidString)
		}
		try! ParserTests.XMLSample.write(to: tempURL, atomically: true, encoding: .utf8)
		defer {
			try! FileManager.default.removeItem(at: tempURL)
		}
		
		let parser = JUnit.Parser(url: tempURL)
		var result: JUnit.Model.TestSuites? = nil
		do {
			result = try parser.parse()
		}
		catch {
			XCTFail("caught exception parsing JUnit: \(error)")
			return
		}
		
		guard let suites = result else {
			XCTFail("failed to get data from parse()")
			return
		}
		
		XCTAssertEqual(suites.failureCount, 3)
		XCTAssertEqual(suites.testCount, 6)
		XCTAssertEqual(suites.name, "BuildMuleTests.xctest")
		XCTAssertEqual(suites.suites.count, 2)
		
		XCTAssertEqual(suites.suites[0].name, "BuildMuleUITests.BuildMuleUITests")
		XCTAssertEqual(suites.suites[1].name, "BuildMuleTests.BuildMuleTests")

		let failingSuite = suites.suites[1]
		XCTAssertEqual(failingSuite.name, "BuildMuleTests.BuildMuleTests")
		XCTAssertEqual(failingSuite.failureCount, 3)
		XCTAssertEqual(failingSuite.cases.count, 5)
		
		let failingCase = failingSuite.cases[0]
		XCTAssertEqual(failingCase.name, "testDoubleFailure")
		XCTAssertEqual(failingCase.failures.count, 1)
		XCTAssertEqual(failingCase.failures[0].message, "failed - first of two intentional fails")
		XCTAssertEqual(failingCase.failures[0].content, "Tests/BuildMuleTests/BuildMuleTests.swift:35")
	}
	
	
	static var allTests = [
		("testParsing", testParsing),
		]
	
}


extension ParserTests {
	
	static let XMLSample = """
<?xml version='1.0' encoding='UTF-8'?>
<testsuites name='BuildMuleTests.xctest' tests='6' failures='3'>
  <testsuite name='BuildMuleUITests.BuildMuleUITests' tests='1' failures='0'>
    <testcase classname='BuildMuleUITests.BuildMuleUITests' name='testExample' time='1.924'/>
  </testsuite>
  <testsuite name='BuildMuleTests.BuildMuleTests' tests='5' failures='3'>
    <testcase classname='BuildMuleTests.BuildMuleTests' name='testDoubleFailure'>
      <failure message='failed - first of two intentional fails'>Tests/BuildMuleTests/BuildMuleTests.swift:35</failure>
    </testcase>
    <testcase classname='BuildMuleTests.BuildMuleTests' name='testDoubleFailure'>
      <failure message='failed - second of two intentional fails'>Tests/BuildMuleTests/BuildMuleTests.swift:36</failure>
    </testcase>
    <testcase classname='BuildMuleTests.BuildMuleTests' name='testExample' time='0.001'/>
    <testcase classname='BuildMuleTests.BuildMuleTests' name='testPerformanceExample' time='5.298'/>
    <testcase classname='BuildMuleTests.BuildMuleTests' name='testSingleFailure'>
      <failure message='failed - intentional fail'>Tests/BuildMuleTests/BuildMuleTests.swift:31</failure>
    </testcase>
  </testsuite>
</testsuites>
"""

}
